FROM python:3.8-buster

ARG UMAP_VERSION=1.2.1

ENV PYTHONUNBUFFERED=1 \
    UMAP_SETTINGS=/srv/umap/settings.py

# Create a user account and group to run uMap
RUN mkdir -p /srv/umap/static /srv/umap/uploads && \
    groupadd --gid 10001 umap && \
    useradd --no-create-home --uid 10001 --gid 10001 --home-dir /srv/umap umap

# Install dependencies
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        binutils \
        libproj-dev \
        gdal-bin \
        build-essential \
        curl \
        libpq-dev \
        postgresql-client \
        gettext \
        libffi-dev \
        libtiff5-dev \
        libjpeg62-turbo-dev \
        zlib1g-dev \
        libfreetype6-dev \
        liblcms2-dev \
        libwebp-dev \
        unzip && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /srv/umap

COPY requirements-docker.txt .

# Get uMap
RUN pip install umap-project==${UMAP_VERSION}

# Install Docker dependencies
RUN pip install -r requirements-docker.txt

COPY entrypoint.sh .
COPY uwsgi.ini .

RUN chmod +x ./entrypoint.sh
RUN chown -R 10001:10001 /srv/umap

USER umap

EXPOSE 8000

ENTRYPOINT [ "/srv/umap/entrypoint.sh" ]
CMD [ "uwsgi", "--ini", "uwsgi.ini" ]
